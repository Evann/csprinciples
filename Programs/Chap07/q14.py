def sum_evens(from_num, to_num):
    # Step 1: Initialize accumulator
    sum = 0
    # Step 2: Get data
    numbers = range(from_num, to_num + 1, 2)

    # Step 3: Loop through the data
    for number in numbers:
        # Step 4: Accumulate
        sum += number

    # Step 5: Return result
    return sum

# Step 6: Print the result of calling the function
print(sum_evens(6, 10))
