## AI
* AI can be used for image language translation, there was an app that Google later bought.
* AI reduces costs and automates.
* It affects how we abuse tech.
* Someone was able to fake a traffic jam in google maps using 99 phones.
* You can put stickers on stop signs to trick self-driving cars.

## Neural Networks
* It is like how the human brain processes info.
* You have nodes for each bit.
* You have an output and an input.
* You keep retraining it till it is correct.
* Convolutions average the color value
* People retrain GPT by replacing the last few layers.
* Tensorflow is a library for AI.
* Recursion can gerenate new content.
* A seed is an input.

## ChatGPT
* A transformer process the whole input. 
* ChatGPT is an autocomplete.
* It does not use unexpected words, which is why you can detect it.
* AI makes a lot of mistakes.

