product = 1  # Start with multiplication identity
num = 1
while num < 11:
    product = product * num
    num += 1
print(product)
