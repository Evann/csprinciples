from gasp import *
import random

def place_robot():
    global robots, robots_x, robots_y, robots_length
    robots = []
    robots_x = []
    robots_y = []
    for i in range(robots_length):
        robots_x.append(random.randint(0, 63))
        robots_y.append(random.randint(0, 47))
        robots.append((Box((10 * robots_x[i], 10 * robots_y[i]), 10, 10, filled=False)))

def place_player():
    global player_x, player_y, player_shape
    player_x = random.randint(0, 63)
    player_y = random.randint(0, 47)

def move_player():
    global player_x, player_y, player_shape, reset
    reset = False
    while True:
        key = update_when('key_pressed')
        if key == 'r':
           safe_place_player(False)
           reset = True
        elif key == 'q' and player_y < 47 and player_x > 0:
            player_x -= 1
            player_y += 1
        elif key == 'w' and player_y < 47:
            player_y += 1
        elif key == 'e' and player_y < 47 and player_x <= 63:
            player_x += 1
            player_y += 1
        elif key == 'a' and player_x > 0:
            player_x -= 1
        elif key == 'd' and player_x < 63:
            player_x += 1
        elif key == 'z' and player_y > 0 and player_x > 0:
            player_x -= 1
            player_y -= 1
        elif key == 'x' and player_y > 0:
            player_y -= 1
        elif key == 'c' and player_y > 0 and player_x < 63:
            player_x += 1
            player_y -= 1
        else:
            continue
        break
    move_to(player_shape, (10 * player_x + 5, 10 * player_y + 5))

def move_robots():
    global player_x, player_y, player_shape, robots, robots_x, robots_y
    for i in range(len(robots)):
        if robots_x[i] > player_x and robots_y[i] < player_y:
            robots_x[i] -= 1
            robots_y[i] += 1
        elif robots_x[i] < player_x and robots_y[i] < player_y:
            robots_x[i] += 1
            robots_y[i] += 1
        elif robots_x[i] > player_x and robots_y[i] > player_y:
            robots_x[i] -= 1
            robots_y[i] -= 1
        elif robots_x[i] < player_x and robots_y[i] > player_y:
            robots_x[i] += 1
            robots_y[i] -= 1
        elif robots_y[i] < player_y:
            robots_y[i] += 1
        elif robots_y[i] > player_y:
            robots_y[i] -= 1
        elif robots_x[i] < player_x:
            robots_x[i] += 1
        elif robots_x[i] > player_x:
            robots_x[i] -= 1
        move_to(robots[i], (10 * robots_x[i] + 0, 10 * robots_y[i] + 0))
        if robots_x[i] == player_x and robots_y[i] == player_y: end_game(False)
                    
def remove_robot(robot_index):
    global score
    score += 1
    print(f'score: {score}')
    player_x, player_y, player_shape, robots, robots_x, robots_y, finished
    trash = (Box((10 * robots_x[robot_index], 10 * robots_y[robot_index]), 10, 10, filled=True))
    robots.pop(robot_index)
    robots_x.pop(robot_index)
    robots_y.pop(robot_index)
    if len(robots) == 0: end_game(True)

def end_game(win):
    global player_x, player_y, player_shape, robots, robots_x, robots_y, finished, win_1
    win_1 = False
    clear_screen()
    if win: win_1 = True
    else: Text('You Lose', (320, 240), color=color.BLACK, size=40)
    finished = True

def safe_place_player(create):
    global player_x, player_y, player_shape, robots, robots_x, robots_y
    while True:
        place_player()
        for i in range(len(robots)):
            if robots_x[i] == player_x and robots_y[i] == player_y:
                redo = True
                break
            else: redo = False
        if redo == False: break
        else: continue
    if create == True: player_shape = Circle((10 * player_x + 5, 10 * player_y + 5), 5, filled=True)
    else: move_to(player_shape, (10 * player_x + 5, 10 * player_y + 5))

def check_collisions():
    remove_robots = []
    for i in range(len(robots)):
        for o in range(len(robots)):
            if i == o: continue
            if robots_x[i] == robots_x[o] and robots_y[i] == robots_y[o]:
                if i not in remove_robots: remove_robots.append(i)
                if o not in remove_robots: remove_robots.append(o)
    for i in range(len(remove_robots)):
        remove_robot(remove_robots[i]-i)

if __name__ == '__main__':
    while True:
        score = 0
        robots_length = 4
        while True:
            begin_graphics(width=640, height=480, title="Robots", background=color.WHITE)
            finished = False
    
            place_robot()
            safe_place_player(True)
    
            move_robots()
    
            while not finished:
                move_player()
                if reset == True: continue
                move_robots()
                check_collisions()
            if win_1 == False: break
            else:
                clear_screen()
                robots_length += 2
        again = input('Would you like to play again (y/n)? ')
        if again != 'y'.lower():
            break
