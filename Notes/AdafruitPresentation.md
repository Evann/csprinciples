# Circut Playground Express

- Backround
- Our Project

## Background
The Circut Playground Express is a microcontroller. Microcontrollers are like small computers.

**Components of the Circut Playground Express:**
- sound sensor
- light sensor
- mini speaker
- motion sensor

The Circut Playground Express has a USB interface, and can be used to emulate a keyboard.

By emulating a keyboard, you can do some pranks on people, or hack people. You can start a reverse shell, by making the target computer connect to you. A reverse shell lets you send commands that run on the target computer.

## Our Project
For the project, as a group we made a bunch of trolls using the keyboard function to make things happen on the computer that it is connected to. 

For example, when we plug in this Circuit Playground, you can see that it uses the keyboard to take over the computer (sort of) and then it connects to the home computer of the person who coded it. Using this, we can make popups appear on the person's computer screen. 
We do this by using bash.

Another project we made was this, where we use the same keyboard function as the last project, except this one has a very devious troll behind it. (plug it into a computer) As you just witnessed, we used a Circuit Playground Express to rickroll all of you. This works by doing the same keyboard things as before, and then it opens up a link to a rickroll, and the video plays. 
