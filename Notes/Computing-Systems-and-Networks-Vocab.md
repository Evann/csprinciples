## Computing Systems and Networks Vocabulary

- Bandwidth
  >The max speed of internet
- Computing device
  >A device that can preform computations
- Computing network
  >Many computing devices that can communicate with each other
- Computing system
  >A combination of hardware, software, and data that are used to perform computations and process data.
- Data stream
  >A flow of data that is transmitted over a network in real datime.
- Distributed computing system
  >A system in which multiple computing devices work together to perform a task, where each device handling a portion of the computation.
- Fault-tolerant
  >The ability of a system to continue functioning even if one or more components fail.
- Hypertext Transfer Protocol (HTTP)
  >A protocol used for transmitting data over the internet, mainly used for web pages.
- Hypertext Transfer Protocol Secure (HTTPS) 
  >An extension of HTTP that provides secure communication over the internet.
- Internet Protocol (IP) address
  >A unique numerical identifier assigned to every device connected to the internet.
- Packets
  >Data that is transmitted over a network.
- Parallel computing system
  >A computing system in which multiple processors work together to perform a task in parallel, which gives faster processing.
- Protocols
  >A set of rules and standards that set the communication between devices over a network.
- Redundancy
  >The duplication of data or components in a system to provide backup in case of failure.
- Router
  >A device that connects multiple networks and directs traffic between them.
