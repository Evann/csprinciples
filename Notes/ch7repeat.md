# Computers Can Repeat Steps Notes
October 19 2022

---
### Reapting Steps

In a procces, repeating steps helps.

Instead of having to write the same things lot of times,
a loop can be used.

### Reapting with Numbers

`for` loops is a type of loop that repeats a certain amount of times.

`for` loops end in a ':' to make an indentation,
anything in the indentation is repeated.
 
`for` loops can be used to repeat for each item in a list.

### What is a List?

A list can hold multiple items, 
they begin with a `[` and ends in a `]`.

The items are seperated by a `,`.

Each item has a order in the list.

**Example of a list**  
`[1, 2, 3, 4, 5]`

### The Range Function

The `range` function is used in a `for` loop to repeat a number of times.
In the function intigers are put in depeneding how how many times you want to repeat.

After the `for` you put a variable name, this variable is local to the for loop.
The variable will equal to the current position of the what you put after `in`  

In the `range` function  if you put 2 intigers as it will start at the first parameter,
and end as the second

**Example of a `for` loop with a `range` function**  
`for i in range(3)`
