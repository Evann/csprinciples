from turtle import *
space = Screen()
t = Turtle()

def rectangle(width, height):
    for i in range(2):
        t.left(90)
        t.forward(height)
        t.left(90)
        t.forward(width)

rectangle(100, 50)
