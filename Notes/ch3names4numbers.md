# Chapter 3: Names for Numbers Notes
September, 15, 2022

---
### Asigning a Name
Variables are values associated with names, 
they are stored in the computers memory. 
They are made in python by putting a name before a equals sign, 
and putting the value after it. 

Python variable names must start with a letter, or underscore, 
and it can't be a reserved word.

**Reserved Words**\
and, def, elif, for, if, import, in, not, return, while 

**Questions**
![alt text](images/Chap3Notes1.png "Question 1")
![alt text](images/Chap3Notes2.png "Question 2")  

---

### Expressions

The value of a variable can be a math expression, and the answer will 
be stored in the variable.

The modulo operator is a "%", and will return the remainder when you 
divide the numbers.

---

### Summary of Expression Types

**Symbol**|**Artithmetic Meaning**
:-----:|:-----:
+|Addition
*|Multiplication
/|Division
//|Floor dvision
%|Modulo
-|Subtraction

**Questions**  
![alt text](images/Chap3Notes3.png "Questions 3-5")

---

### How Expressions are Evaluated

The order that they are evaluated is the same as math.

---

### Summary

Variables names  must follow rules.

If you assign a variable a math problem, it will equal the answer.
