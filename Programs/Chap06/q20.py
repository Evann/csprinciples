import datetime
now = datetime.datetime.now()

def time(hour, minute, add_hour, add_minute):
    new_hour = hour + add_hour
    if hour + add_hour > 12: new_hour = new_hour - 12
    if new_hour == 0: new_hour = 12
    new_minute = minute + add_minute
    if new_minute > 60:
        new_minute = new_minute - 60
        new_hour += 1
    return (new_hour, new_minute)

new_time = time(now.hour-12, now.minute, int(input('How many hours do you want to add? ')), int(input('How many minutes do you want to add? ')))
print(f'{new_time[0]}:{new_time[1]}')
