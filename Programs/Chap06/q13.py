def num_miles_func(tank_capacity, amount_left, miles_per_gallon):
    num_gallons = tank_capacity * amount_left
    num_miles = num_gallons * miles_per_gallon
    return num_miles

print(num_miles_func(10, 0.25, 32))
