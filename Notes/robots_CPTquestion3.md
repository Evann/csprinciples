# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
>The purpose of the program is for entertainment.
>
>
2. Describes what functionality of the program is demonstrated in the
   video.
>It opens a graphics window, and has you play a game called robots
>As you go on, more robots will spawn.
>
3. Describes the input and output of the program demonstrated in the
   video.
>The input is keys on the keyboard being pressed.
>The ouput is the movement of the player, and robots. Also if a robot is kill, or if you win or lose.
>

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
>![robots-list-1](images/robots-list-1.png)
>
>
2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
>![robots-list-2](images/robots-list-2.png)
>
>

## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
> On line 35, the name of the list is `robots`.
>
>
2. Describes what the data contained in the list represent in your
   program
>It stores the position in line 39 and 40, and shape of each robot in line 42.
>
>
3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
>The list is necessary, because as go on there will levels with more robots. 
>Without the list, it wouldn't go on forever.
>

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
>![robots-function-1](images/robots-function-1.png)
>
>
>
>
2. The second program code segment must show where your student-developed
   procedure is being called in your program.
>![robots-function-2](images/robots-function-2.png)
>
>
>

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
>It check if a robot is on the same position as another robot.
>The procedure allows for the robots to crash.
>
>
4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
>The procedure takes in a robot, and goes through every robot in `robots` in line 114.
>If the robot is the same as the current robot in the for loop, it returns False in line 115 and 116.
>The it checks if the robot has the same position on the board as the current robot in the for loop in line line 117.
>If it does, it returns the robot with the same position in line 118.
>If none of those things happen, it will retun false in line 119.
## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.
>
> First call:
>![robots-function-2](images/robots-function-2.png)
>
>
>
> Second call:
>![robots-function-3](images/robots-function-2.png)
>(It is in a for loop)
>
>
2. Describes what condition(s) is being tested by each call to the procedure
>
> Condition(s) tested by the first call:
>A robot in `robots` for line 139
>
>
>
> Condition(s) tested by the second call:
>A robot in `robots` for line 139
>
>
>
3. Identifies the result of each call
>
> Result of the first call:
>Depends on position of for loop for line 139.
>
>
>
> Result of the second call:
>Depends on postion of for loop for line 139.
>
>
>
