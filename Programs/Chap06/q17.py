from turtle import *
space = Screen()
t = Turtle()

def triangle(s1, s2, s3):
    t.forward(s1)
    t.left(120)
    t.forward(s2)
    t.left(120)
    t.forward(s3)

triangle(100, 100, 100)
