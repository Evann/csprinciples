## Data Vocab
- abstraction
    >To make something less complicated, like adding a GUI
- analog data
    >Data that is infinite. The real world is analog
- bias
    >Where data is not accurate, because of an unfair reason
- binary number system
    >How numbers are repersented in binary
- bit
    >Smallest unit for digital computer, can be a 1 or 0
- byte
    >8 bits
- classifying data
    >organizing data into categories
- cleaning data
    >Fixing or removing incorrect data
- digital data
    >Data that is limited, computers are digital
- filtering data
    >foucusing on certian data
- information
    >facts about something
- lossless data compression
    >Compression that does not alter the uncompression form
- lossy data compression
    >Compression that does alter the uncompression form
- metadata
    >Info about the data
- overflow error
    >An error that happens when data is too big
- patterns in data
    >Data that repeats in a recognizable way, used in data mining
- round-off or rounding error
    >An that happens when a computer rounds the number
- scalability
    >How easily a system is able to grow to handle more work