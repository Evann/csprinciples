# Legal and Ethical Concerns: Copyright and Creative Commons

### Table of Contents
* [Copyright](#copyright)
* [Licenses](#licenses)
* [Creative Commons](#creative-commons)
* [Types of Software Licenses](#types)
* [No Copyright or License](#none)

 <div id='copyright'/>

## Copyright

Copyright is legal right given to a person or orgination that allows the use a piece of literacy, art, or music, without other people or organizations being able to use it. Copyright is automaticly given to the creator.

<div id='licenses'/>

## Licenses
A license gives permission from the copyright holder to do things to the work. The things you can do depeneding on what the licenses says. There are many different licences that set those permissions.

<div id='creative-commons'/>

## Creative Commons
One licenses is the Creative Commons License. It allows creators to grant permission to everyone in the world to use their work in certain ways. Creative Commons is not recommended for software.

<div id='types'/>

## Types of Software Licenses
- **Permissive**: Have minimal restrictions on how the software can be modified or redistributed. The most popular open source license.
  >BSD, MIT, and Apache

- **Weak copyleft**: It is designed to allow linking to open source libraries with little obligation.
  >LGPL, MPL, CDDL, and Eclipse.

- **Copyleft**: restricted licenses that are less commercial-friendly than other licenses. These liceses allow  developers to modify the licensed code.
  >GPL, and AGPL

- **Commercial or proprietary**: Generally used for commercial software where the copyright holder is asserting express conditions with respect to the rights being granted, and for instance, doesn’t want the code to be shared, reverse-engineered, modified, redistributed, or sold
- **Dual**: An increasingly common business model is dual-licensing, using both a copyleft or other form of open but restrictive license, and a commercial license.
  >AGPL, and SSPL

<div id='none'/>

## No Copyright or License
- **Public domain**: Copyright does not apply to works in the public domain. This is caused by the copyright from being expiered. Anyone can modify and use such software without any restrictions.
- **Unlicensed**: Code that doesn’t have an explicit license is not de facto in the public domain. This happens when copyright holder doesn't add a license.
