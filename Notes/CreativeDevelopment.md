## Creative Development Vocabulary

- code segment
    >A section in the computer's memory that is where the program is
- collaboration
    >Working together
- comments
    >In programming, they are notes in a file that are ignored when the file is ran 
- debugging
    >To get rid of errors in the program
- event-driven programming
    >Where is flow of the program is decided by events
- incremental development process
    >Where you create software in small pieces
- iterative development process
    >Breaking down the software into smaller pieces
- logic error
    >Where a computer does math wrong
- overflow error
    >Where the data is too big for the computer
- program
    >A set of instructions for a computer
- program behavior
    >What the program does
- program input
    >What is put into the program
- program output
    >What is put out of the program
- prototype
    >A model for something that is made before it is produced more
- requirements
    >What you need
- runtime error
    >An error that happens while the program is being executed
- syntax error
    >A error that is casued by writing the code wrong
- testing
    >Seeing how things work by using that thing
- user interface
    >How a user uses the program

## Errors

**Syntax Error:**
```
for i in range(10)
    print(i)
```

**Syntax Error:**
```
# Add the numbers from 1 to 10 and print the result
total = 0
for num in range(10)
    total += num 
print(total)
```

**Runtime Error:**
```
nums = [3, 5, 8, 0, 9, 11]
result = 1
for num in nums:
    result = result / num
print(result)
```
