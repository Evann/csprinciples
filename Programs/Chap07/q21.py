def function(number):
    product = 0
    sum = 0
    for i in range(2, number+1, 2): product+=i
    for i in range(1, number+1, 2): product+=i
    return product-sum
print(function(20)/2)
