# Intro to Networking Notes
- [Chapter 1](#ch-1)
- [Chapter 2](#ch-2)
    - [Link Layer](#ch-2-link)
    - [Internetwork Layer (IP)](#ch-2-ip)
    - [Transport Layer (TCP)](#ch-2-tcp)
    - [Application Layer](#ch-2-app)
- [Chapter 3](#ch-3)
- [Chapter 4](#ch-4)
- [Chapter 5](#ch-5)
- [Chapter 6](#ch-6)
- [Chapter 7](#ch-7)
- [Chapter 8](#ch-8)

<div id='ch-1'/>

## Chapter 1

The technologies for the internet were started to be created in the 1960s

Early telephone systems worked by a human operator connecting two wires so that two people could talk to each other. the wire would be disconnected when the call was finished.

Before telephone companies used fiber optics, long-distance calls were expensive.

At the start of connecting computers, two computers were wired together to communicate. If both computers were in different buildings, then wires had to be leased from telephone companies.

In the 1970s and 80s, at universities, people wanted to send data to each other using computers. What was done was a chain of computers would store and forward the message that was sent. Although it could take minutes, hours, or days to send.

Packets are pieces of messages. The advantage is the time the message takes to send would be more consistent since each packet is sent separately and are the same size. It also reduced the storage of the computers needed in the network. Computers that specialized in moving packets were being made called IMPs. They would become routers, as they routed packets to a destination.

To connect a computer to a network, it just has to be connected to one router. Multiple connected computers in one location are called LAN. Computers in the LAN can send data through the router into the WAN.

Each computer is given an address, as an identifier in the network. For each packet, the offset needed to be added to give the position of the packet in the message.

<div id='ch-2'/>

## Chapter 2 

There are four layers to the internet, called The Four-Layer TCP/IP Model, the Link, Internetwork, Transport, and Application layer.

<div id='ch-2-link'/>

### Link Layer

The Link Layer connects your computer to the LAN and moves data across the first hop/connection.

Wireless networking is the most common Link layer, WiFi and cellular are examples of those.

The Link layer solves how to send and encode data across. The radio frequencies used have to be agreed upon, and for wired connection, like fiber optics, the frequencies of light have to be agreed on.

How to cooperate with other computers that send data at the same time has to be agreed on. Packets make sharing easier. Each computer gets a turn at getting a packet sent. CSMA/CD detects when multiple computers send data at the same time.

<div id='ch-2-ip'/>

### Internetwork Layer (IP)

When a packet is going to the internet, it goes to a router, which is the first link. The packet sent has a source address and a destination address.

The router looks at the destination address to see how to send it to the destination. The routers along the way also do their best to get the packet to its destination.

Packets can have delays, so Routers send special messages to each other. Routers are smart and can switch routes because of an outage.

<div id='ch-2-tcp'/>

### Transport Layer (TCP)

Sometimes packets are lost, delayed badly, or out of order, TCP solves that problem.

Each packet has the source computer's address and an offset. The offset can be used to reconstruct the message. It is sent how much of the message has been received and reconstructed.

If the destination computer finds that packets are missing, it asks the source computer to resend the data.

The sending computer must store a copy of what is being sent. It can't delete parts until the destination computer tells that it received the packet. The amount of data the source computer waits for the acknowledgment is the "window size."

If the window size is too small, the transmission is slow, but if it is too big, it can overload routers. The window size is balanced depending on the data speeds.

<div id='ch-2-app'/>

### Application Layer

The Application Layer defines the networked application that uses the network connections.

In the 1980s, the first networked applications allowed users to log in to remote connections, transfer files, send mail, and do chats.

In the early 1990s, as the internet got bigger, the World Wide Web application was being developed. The web allowed reading and editing networked hypertext docs with images. Today the web is the most common Application Layer.

One-half of the application is the server and waits for incoming connections. The other half is the client. Firefox and Chrome are examples of web client applications.

The Uniform Resource Locators (URLs) are what your client is connected to.

The application protocol also must be defined, which describes how the messages are sent to the two halves. An example would be HTTP and HTTPS.

<div id='ch-3'/>

## Chapter 3

The Link Layer is the lowest layer.

The Link Layer transmits data with a wire, a fiber optic cable, or a radio signal. The Link Layer transmits data a part of the way to the destination computer.

Cellular, satellites, and fiber optics can transmit data over long distances.

WiFi is sent and received using small low-powered radios that can only send up to 300 meters away. All computers connected to the router/base station can hear all the sent packets by other computers, a way of knowing which is needed.

Every Wifi radio in each device has a unique serial number, called the MAC address: `0f:2a:b3:1f:b3:1a`, Binary: `00001111:00101010:10110011:00011111:10110011:00011010.` 
It is a 48-bit serial number that uses hexadecimal.

When a computer connects to a network, it needs to find out the MAC address of the router. The computer tries again if there is no reply, your computer will have a "no wifi" icon.

The MAC address differentiates which packets are for which computers.

There are many computers on the same radio frequency when using WiFi. Carrier Sense makes it so computers take turns transmitting a packet. 

Computers might try to take turns at the same time, which can corrupt your data, so Collision Dectition makes the computers stop transmitting data. The computers then wait for a random amount of time

The name for these is “Carrier Sense Multiple Access with Collision Detection” or CSMA/CD. Wired Ethernet, cellular telephone data, and Short Message Service (SMS/Texting) all use this too.

Sometimes when a link layer has many computers, each computer is given a token for an amount of time, that tells when the computer can transmit data. The token is moved when the computer has nothing to send.

<div id='ch-4'/>

## Chapter 4

The Internetwork Layer sends data around the world.

The data needs to travel across multiple hops and networks.

The routers look at every destination address to decide where to route it. The IP address is what the address is.

A MAC address cannot be used to route packets across multiple networks.

Every computer is assigned an IP address. An IPv4 address is four numbers up to 255 separated by commas: `158.59.225.115`, Binary: `10011110.00111011.11100001.01110011`.

IPv4 addresses are running out. IPv6 addresses are longer: `2001:0db8:85a3:0042:1000:8a2e:0370:7334`

IPv4 addresses have a “Network Number”. It is the first 2 numbers. The "Host Identifier" is the last two numbers. There can be 65,536 possible Host Identifiers per Network Number, because 256^2=65,536. It wouldn't be 255^2 because 0 can be used.

A lot of computers in the same network use the same Network Number and different Host Identifiers. Routers don't have to see the entire address now.

Each router still needs to learn the path to each network number it would encounter.

A new router would only know a few preconfigured routes. If it does not know the route it asks neighboring routers. A router slowly builds a map of network numbers, called a "routing table".

When one of the links fail, it discards all entries on its routing table that were being routed on that link and tries to find a different path. Packets move more slowly as routing tables are rebuilt.

There should be two routes for a network.

Routers are always looking to find the best path.

Routers do not know the full path the packet will take. Most computers have a tool called "traceroute"/"tracert" as a diagnostic. Although it is an approximation, and might not be exact.

Each packet has a number called the Time To Live (TTL), so packets don't get stuck in loops. It starts with 30, and each time it goes to another link the number goes down by 1.

If the TTL reaches zero the router will discard the packet and sends a message to the computer that sent the packet.

Traceroute sends a packet with a TTL of 1, then it sends a packet with a TTL of 2 and so on. Not all routers say that the message was discarded, which is why it is just an approximation.

The ability for a computer to get a new IP address when moved to another network is a protocol called Dynamic Host Configuration Protocol (DHCP).

A computer asks the base station if there is a gateway connected to the network that can get to the internet. If there is, it asks for an IP address, and the router gives a temporary address,

Some OS give the computer an IP starting with 169 if the DCHP didn't respond, but the computer would not be able to connect to the internet.

Addresses that start with 192.168 or 10 are non-routable addresses that can only be used on the local network. This is due to the router using Network Address Translation (NAT). The gateway replaces the address with a routable address.

An ISP will give a range of IPs that can be given to computers in the network. The portion is received from a higher-level internet service provider.

There are five Regional Internet Registries (RIRs). Each of the five gives IPs to major geographic areas. 

Before it was thought that IPv4 addresses were enough, but now there are way more computers connected to the internet. IPv6 addresses were made, and are much longer than IPv6.

<div id='ch-5'/>

## Chapter 5

The Domain Name System (DNS) lets you use domains instead of IPs.

If your computer changes IPs, the domain will still be the same.

The International Corporation for Assigned Network Names and Numbers (ICANN) is the top origination for domains, they can choose TLDs.

When a domain is acquired, subdomains can be assigned.

For domains, the more specific part is on the left side, unlike IPs which are on the right side.

Anyone can buy a domain from registrars.

<div id='ch-6'/>

## Chapter 6

The transport layer makes it so there are no lost or out-of-order packets because the Internetworking layer does not ensure that.

A packet header is given to packets. It contains the link header, the IP Header, and the TCP header, the data is after the header.

| Link Header | IP Header         | TCP Header     | Data Packet |
|-------------|-------------------|----------------|-------------|
| From \| To  | From \| To \| TTL | Port \| Offset | ...         |

Once a packet gets to a link, a new link header is replaced. The IP Header has the first source computer and the final destination computer.

The TCP header indicates the offset of the packet and the port.

When the packet arrives out of order, the packet is put in a buffer until the previous packets arrive.

The sending computer only sends some data until the destination sends an acknowledgment. The time is called the window size.

The window size is increased when the acknowledgment is sent slowly, and the opposite happens when the acknowledgment is sent fast. 

If no packet is sent for enough time, the receiving computer tells the sending computer where it is in the message. The sending computer then resends the packets.

The sending computer must hold on to all of its data until an acknowledgment is received.

The computer that starts the connection is the client and the computer that responds to the connection is the server.

A port makes sure it is connecting to the right server application. A server listens on a specific port. The port for HTTP is 80.

<div id='ch-7'/>

## Chapter 7

The Application layer is what defines the software used, like email, web, or networked video games.

A client and server are needed for a networked application.

The server runs on the internet and has the info the user uses. The client makes the connection to the server and retrieves the info.

A web server program is always waiting for a client to connect.

The rules/protocol for communication is set.

HTTP is a protocol used for the web.

The telnet application can make HTTP GET requests by manually entering things based on the HTTP protocol.

A header is given in the HTTP response which includes a status code.

Examples are 200 means it went well, 404 means the requested doc was not found, and 301 means the doc has been moved to a new location.

There are ranges for the status codes, 2XX are for success, 3XX are for redirecting, 4XX codes say the client did something wrong, and 5XX codes say the server did something wrong.

The Internet Message Access Protocol (IMAP) is a protocol for email.

The ability to start and stop the sending application so data can be sent quickly is called flow control. The transport layer is responsible for this.

Programming languages usually have libraries to make it simple to make networked applications.

The socket library is a networking library for Python.

<div id='ch-8'/>

## Chapter 8

Networks were small and secure in the early days.

If there were no unwanted connections, it was felt there was no need to protect the network.

Privacy of data wasn't worried about until the late 1980s when the internet became mainstream. Especially since online shopping was a thing.

Physical links should be in secure locations so people can't sneak in and monitor them. With WiFi, an attacker can just scan the radio waves and intercept packets.

Therefore, encryption is the only reasonable solution.

Ciphertext is encrypted and decrypted with a secret key.

Asymmetric key encryption works with one key encrypting it, and one key decrypting it.

The computer that is receiving the data chooses both of the keys. The encryption key is sent to the sending computer. The encrypting key is called the public key because it is close to impossible to decrypt data.

SSL/TLS was added to existing internet protocols. It is between the application and transport layers. If encryption is requested, the Transport layer encrypts the data before breaking it into packets.

HTTPS is encrypted HTTP.

There is a small cost to encrypt and decrypt data, so for a while, it was only used for sensitive data. Now almost everything is encrypted.

There is a problem with knowing if the public key you received is from the organization it is from.

A key signed by a Certificate Authority (CA) is sent by the server. Browsers come preinstalled with knowing well-known CAs. 