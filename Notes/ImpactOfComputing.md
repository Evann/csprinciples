## Impacts of Computing Vocabulary

- asymmetric ciphers
>Where there is a public key and private key to encrypt and decrypt
- authentication
>To check if they are who they are
- bias
>To unfairly choose something over the other
- Certificate Authority (CA)
>An organization that validates keys with certificates
- citizen science
>Where the public collabrates to research
- Creative Commons licensing
>A license where anyone can the the work under the licence
- crowdsourcing
>Getting information from a lot of people
- cybersecurity
>Being safe with computers
- data mining
>To process data to find patterns
- decryption
>To make a message usable after it was encrypted
- digital divide
>The unequal access to technology
- encryption
>To hide to messages from being read
- intellectual property (IP)
>Where you own an idea
- keylogging
>To store key presses
- malware
>Software the user did not intend to run
- multifactor authentication
>Using multiple methods to authenicate
- open access
>To release your research to everyone
- open source
>Where everyone can see the source code
- free software
>Software that can be used in anyway
- FOSS
>Free and open source software
- PII (personally identifiable information)
>Data that can link to you as a pereson
- phishing
>Sending a message to maliciously gain info to the reciver of the message
- plagiarism
>Copying other work exactly
- public key encryption
>Encryption where a public key is used
- rogue access point
>A malicious wifi access point
- targeted marketing
>Marketing that only show for people that match their data with the product
- virus
>Malware where malicious code is inserted into programs
