sum = 0
count = 0
value = 1
message = "Enter an integer or a negative number to stop"
while int(value) > 0:
    value = int(input(message))
    print("You entered " + str(value))
    sum = sum + value
    count = count + 1
print("The sum is: " + str(sum) +
      " the average is: " + str(sum / count))
