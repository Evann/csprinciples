from random import randint #imports the randint function from the random libray
i = 1 #i is the question number
correct = 0 #correct is how many you got correct
questions = 10 #how many questions there are; can be changed
min_num = 1 #The minmiun number a factor can be; can be changed
max_num = 10 #The maximum number a factor can be; can be changed
while i <= questions: #start the loop; repeat for the number of questions
    num1 = randint(min_num,max_num) #num1 is a random number 1-10
    num2 = randint(min_num,max_num) #num2 is a random number too
    answer = num1*num2 #the answer is num1*num2
    question  = f'Question {i}: What is {num1} times {num2}? ' #the question is what is num1*num2
    response = input(question) #ask the question
    try: int(response) #check if the response is a number
    except: #if not
        print("Please type a number") #say to type a number
        continue #loop again
    i+=1 #make next loop next question
    if int(response) == answer: #check if the response is correct
        correct+=1 #add 1 correct answer
        print("That's right - well done") #say well done
    else: print(f"No, I'm afraid the answer is {answer}") #else, tell them its not correct
print(f'You got {correct}/{questions} questions correct') #say how many were correct
