from turtle import *
space = Screen()
t = Turtle()

def square(turtle, distance, angle, c1, c2, c3, c4):
    colors = (c1, c2, c3, c4)
    for i in range(4):
        turtle.color(colors[i])
        turtle.forward(distance)
        turtle.left(angle)

square(t, 100, 90, 'red', 'blue', 'green', 'yellow')
