from turtle import *
space = Screen()
turtles = (Turtle(), Turtle(), Turtle(), Turtle())
colors = ('red', 'green', 'blue', 'black')
degrees = 0
for i in range(4):
    turtles[i].color(colors[i])
    turtles[i].left(degrees)
    turtles[i].forward(100)
    degrees += 90
