"""
>>> q02(range(1, 11))
55
"""

def q02(num_list):
    sum = 0
    for number in num_list:
        sum += number
    return sum

if __name__ == "__main__":
    import doctest
    doctest.testmod()
