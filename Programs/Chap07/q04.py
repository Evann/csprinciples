product = 1      # Start with the multiplicative identity
numbers = [1, 2, 3, 4, 5]
for i in numbers:
    product = product * i
print(product)               # Print the result
