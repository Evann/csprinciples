## Edge Computing
* It is computing at the source, and not in the cloud.
* Edge Computing is higher quality and more private.
* The hardware can be specialized

## ESP32
* Is a series of low-powered microprocessors
* There are boards you can buy with cameras on them.

## TinyML
* TinyML is machine learning software that can run on an ESP32.
* DSP filters with parameters
* Neural networks filter with learned weights from training
* CNNs filter by comparing the values of pixels
* Quantization adds fewer levels, making it less precise, but less intensive
* Pruning also makes it less precise and less insensitive
* Edge Impulse is a platform that uses ML and lets you make a model
* Modeling is where you put in the training images
* Mobilenet is a model architecture for tinyml
* Someone day training will be done on edge instead of cloud