## For Loop

`for` loops repeat a the body of the loop a set number of times.

The body of a `for` loop is made by putting it right of the `for` loop's indentation.

## While Loop

`while` loops repeat the body of the loop while an expression is true.

The expression is put the the right of the `while`.

The body of a `while` loop is also made by putting it to the right of the indentation.  
