## How is the APCSP exam scored?
The APCSP exam is scored 1-5, 5 being the best, and 1 being the worst.

**Score**|**Multiple Choice Score**|**Meaning**|**Passing**|**Collage Credit**|**Percentage of Test Takers**
:-----:|:-----:|:-----:|:-----:|:-----:|:-----:
5|60/70|Extremely qualified|✓|✓|10.9%
4|51/70|Well qualified|✓|✓|23.6%
3|32/70|Qualified|✓|?|37.1%
2|N/A|Possibly qualified|✕|✕|19.8%
1|N/A|No recommendation|✕|✕|8.6%

Getting a collage credit with a score of 3 depends on school.

There are different sections of the exam, and worth different amounts.

**Section**|**Grade**
:-----:|:-----:
Multiple-Choice|70%
Perfromance Task|30%

The preformance task is where the student will make a computer program.
They will get 12 hours of class time to complete it.

## On which of the 5 Big Ideas did I score best? On which do I need the most improvement?

**Big Idea**|**Score**
:-----:|:-----:
Creative Development|7/11
Data|6/12
Algorithms and Programming|12/28
Computing Systems and Networks|3/6
Impact of Computing|8/12

I should mostly improve in algorithms and programming.

## What online resources are available to help me prepare for the exam in each of the Big Idea areas?

[CS50 Lectures](https://www.youtube.com/c/cs50)

[codeHS AP CSP Exam Review](https://codehs.com/playlist/ap-cs-principles-exam-review-1780?)

[Khan Academy APCS ](https://www.khanacademy.org/computing/ap-computer-science-principles)
