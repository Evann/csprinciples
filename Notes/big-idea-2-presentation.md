# Converting to Binary
Binary is reperseted by 1s and 0s. Computers are digital, so everything are binary bits.
## Colors to Binary
Computers store colors as three or four integers.  
They are as follows:
* red
* green
* blue
* Transparent Value (optional)

Those integers can then be converted into binary.
## Integers to Binary
In python, integers can be converted to binary using the `bin()` function.

Example:
```
x = 5
print(val(x))
```
That would print `0b101`. The `0b` tells you it is binary.
## Text to Binary
In python, you can convert a single chartecter string into binary using the `ord()` function, and the `bin()` function.

Example:
```
x = "a"
y = ord(x)
print(y)
```
That would print `97`, which is not binary.   
The `ord()` function returns the number representing the unicode code of a specified character.  
You would then use the `bin()` function to convert the number to binary.
```
z = bin(y)
print(z)
```
This is would print `0b1100001`, which is binary.
