def move(turtle, length, angle):
    turtle.forward(length)
    turtle.right(angle)
    turtle.forward(length)
    turtle.right(angle)
    turtle.forward(length)
    turtle.right(angle)
    turtle.forward(length)
    turtle.right(angle)

from turtle import *
space = Screen()
t = Turtle()
move(t, 100, 90)
